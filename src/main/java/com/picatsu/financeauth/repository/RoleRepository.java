package com.picatsu.financeauth.repository;

import java.util.Optional;

import com.picatsu.financeauth.models.ERole;
import com.picatsu.financeauth.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface RoleRepository extends MongoRepository<Role, String> {
  Optional<Role> findByName(ERole name);

  @Override
  <S extends Role> S save(S s);
}
