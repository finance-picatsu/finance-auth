package com.picatsu.financeauth;

import com.picatsu.financeauth.models.ERole;
import com.picatsu.financeauth.models.Role;
import com.picatsu.financeauth.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinanceAuthApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(FinanceAuthApplication.class, args);
	}

	@Autowired
	RoleRepository roleRepository;

	@Override
	public void run(String... args) {

		Role role = new Role();
		role.setId("1");
		role.setName(ERole.ROLE_USER);
		roleRepository.save(role);


		Role role4 = new Role();
		role4.setId("4");
		role4.setName(ERole.ROLE_ADMIN);
		roleRepository.save(role4);


	}

}
