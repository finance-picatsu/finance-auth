#! /bin/bash

./gradlew clean build -x test

JAR_FILE=$(ls build/libs/ | grep "^CEVT")

docker build . --build-arg jar=build/libs/$JAR_FILE -t ezzefiohez/auth
docker push ezzefiohez/auth


curl  -X POST http://146.59.195.214:9000/api/webhooks/9f606ac8-965c-4d05-9f7a-2511ddf439a0
